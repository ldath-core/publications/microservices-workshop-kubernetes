#!/usr/bin/env bash
mkdir public
cp -R images public/
cp microservices-workshop-kubernetes.html public/index.html
cp microservices-workshop-kubernetes.pdf public/
